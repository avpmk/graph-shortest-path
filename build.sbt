name := "graph-shortest-path"
version := "0.0.1"

def fullScalaVersion = "2.13.6"
def javaVersion      = 11

def packagesAllowedForInline = Seq(
  "scala.**",
  "ru.petrovandrei.**"
)

def inlinePackagesFlag = {
  val packages = packagesAllowedForInline
  if (packages.isEmpty) "" else packages.mkString("-opt-inline-from:", ",", "")
}

//todo add dev mode without opt flags for faster local builds
scalaVersion := fullScalaVersion
scalacOptions := Seq(
  s"-target:$javaVersion",
  "-opt:l:method",
  "-opt:l:inline",
  inlinePackagesFlag,
  "-feature",
  "-language:implicitConversions",
  "-Ymacro-annotations",
  "-deprecation",
  "-unchecked",
  "-opt-warnings",
  "-Xlint:_,-byname-implicit",
  "-Xfatal-warnings",
  "-P:silencer:checkUnused"
)

//scalafmtOnCompile := true
//Compile / compile := (Compile / compile).dependsOn(Compile / scalafmtSbt).value
Compile / compile := (Compile / compile)
  .dependsOn(
    Compile / scalafmtCheckAll,
    Compile / scalafmtSbtCheck
  )
  .value

val kindProjectorVersion    = "0.13.2"
val betterMonadicForVersion = "0.3.1"
val silencerVersion         = "1.7.6"

val newtypeVersion = "0.4.4"

addCompilerPlugin("org.typelevel" %% "kind-projector"     % kindProjectorVersion cross CrossVersion.full)
addCompilerPlugin("com.olegpy"    %% "better-monadic-for" % betterMonadicForVersion)
libraryDependencies ++= Seq(
  compilerPlugin("com.github.ghik" % "silencer-plugin" % silencerVersion cross CrossVersion.full),
  "com.github.ghik" % "silencer-lib" % silencerVersion % Provided cross CrossVersion.full
)

def λ[T](f: String => T) = new {
  def apply(xs: String*) = xs map f
}

val scalaLibraryExclusion = λ(ExclusionRule("org.scala-lang", _))("scala-library", "scala-reflect")

def scalaLib(
    n: String,
    v: String,
    modifyModule: ModuleID => ModuleID = identity
) = {
  val createModule: String => ModuleID = n %% _ % v excludeAll (scalaLibraryExclusion: _*)
  λ(createModule andThen modifyModule)
}

def scalaReflect = Seq(
  "org.scala-lang" % "scala-reflect" % fullScalaVersion
)

def newtype = scalaLib("io.estatico", newtypeVersion)(
  "newtype"
)

def all = Seq(
  scalaReflect,
  newtype
)
libraryDependencies ++= all.flatten
