addSbtPlugin("com.eed3si9n"  % "sbt-assembly" % "1.0.0")
addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.4.3")

// for dependencyUpdates task
//addSbtPlugin("com.timushev.sbt" % "sbt-updates" % "0.6.0")

/*
addSbtPlugin("com.github.cb372" % "sbt-explicit-dependencies" % "0.2.16")
addSbtPlugin("ch.epfl.scala"    % "sbt-scalafix"  % "0.9.26")
 */
